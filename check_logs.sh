#!/bin/sh -e

logs() {
  ssh "${target_host}" "docker-compose -f ${remote_path}/docker-compose.yml logs --tail=4"
  # sometimes is nice
  #   ssh "${target_host}" "docker-compose -f ${remote_path}/docker-compose.yml logs --tail=4 -f" &
}

main() {
  . db.d/cfg.env
  nodes="$(dirname db.d/network/*/cfg.env)"
  for node_path in ${nodes}; do
    # load node vars (ssh host)
    . "${node_path}/cfg.env"
    target_host="${remote_user}@${ssh_host}"
    echo "${node_path}"
    logs
  done
}

main

