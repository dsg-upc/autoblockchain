**this service is experimental and serves for experimentation, do not use it in production, right now, security is not a priority**

[TOC]

## requirements

- docker
- docker-compose

## besu quickstart on localhost

```
./bootstrap_besu.sh
docker-compose up
```

notes on being patient:

- key generation is slow
- startup of services is slow

## besu workflow for remote deployment and start

warning/fixme: on remote node is installed to use root user

```
# generates: keys, templates, files needed for the blockchain
./bootstrap_besu.sh
# deploys remotely
./deploy.sh
# a simple command to start/stop remote blockchain
./restart.sh
# a simple command to see the logs
./check_logs.sh
```

## WIP geth quickstart on localhost

given geth inestability, I usually run from scratch with this script

`make geth_restart_clean`

## clean data

make clean_all

see Makefile for other options

### monitoring notes

check target on prometheus: http://localhost:9090/targets

access grafana: http://localhost:3000/dashboards with admin/admin

## template generating

you can directly generate the templates with the following commands

for the genesis json file

```
. genesis.json.template ; echo "${TEMPLATE}"
```

for the enode json file

```
. enodes.json.template ; echo "${TEMPLATE}"
```

for the docker-compose template

```
. docker-compose.yml.template ; echo "${TEMPLATE}"
```

for the prometheus template

```
. monitoring/prometheus/prometheus.yml.template ; echo "${TEMPLATE}"
```

## besu

process Xhelp

```
docker run --rm hyperledger/besu:21.7.4 --Xhelp > xhelp
```

metrics docs https://besu.hyperledger.org/en/stable/Reference/CLI/CLI-Syntax/#metrics-host
