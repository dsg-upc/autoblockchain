#!/bin/sh

set -eu

start() {
  ssh "${target_host}" "docker-compose -f ${remote_path}/docker-compose.yml up -d"
}

stop() {
  ssh "${target_host}" "docker-compose -f ${remote_path}/docker-compose.yml stop"
  yes | ssh "${target_host}" "docker-compose -f ${remote_path}/docker-compose.yml rm"
}

main() {
  . db.d/cfg.env
  nodes="$(dirname db.d/network/*/cfg.env)"
  for node_path in ${nodes}; do
    # load node vars (ssh host)
    . "${node_path}/cfg.env"
    target_host="${remote_user}@${ssh_host}"
    stop
    start
  done
}

main

