#!/bin/sh -e

#set -x

# src https://en.wikipedia.org/wiki/Usage_message
usage() {
  echo "Usage: ${0} -n node_quantity"
  exit 1
}

parse_arguments() {
  # src https://stackoverflow.com/questions/16483119/an-example-of-how-to-use-getopts-in-bash/49573433#49573433
  #   https://pubs.opengroup.org/onlinepubs/9699919799/utilities/getopts.html
  #     If a character is followed by a <colon>, the option shall be expected to have an argument
  while getopts n:g name
  do
    case $name in
      n) nflag=1
         nodes_length="$OPTARG";;
      ?) usage
      exit 2;;
    esac
  done
  if [ -z "$nflag" ]; then
    usage
  fi
  # TODO check n is a number
}

# TODO backups de llaves anteriores con timestamp
# TODO parallelize key generation process, background del proceso? potencial
#   problema de que se haga template sin generación de llaves
gen_keys() {
  # TODO rethink args and vars
  node="${1}"
  if ! [ ${node} -gt 0 ] 2> /dev/null; then
    echo "node should be a number such as 1, 2, 3, etc."
    exit 1
  fi
  dpath_docker="/opt/besu/data"
  dpath_local="$(pwd)/data"
  npath_docker="${dpath_docker}/${node}"
  npath_local="${dpath_local}/${node}"

  mkdir -p "${npath_local}"
  # besu_cmd="besu"   # was this way inside entrypoint.sh
  besu_cmd="docker run \
    -v ${dpath_local}:${dpath_docker}:rw --rm \
      hyperledger/besu:${version}"
  ${besu_cmd} \
    --data-path=${npath_docker} \
    public-key export-address --to=${npath_docker}/pubkey
  ${besu_cmd} \
    --data-path=${npath_docker} \
    public-key export --to=${npath_docker}/enode
  cd ${npath_local}
  mv key privkey
  cd - > /dev/null 2>&1
}

# TODO use a more common function for templates
gen_templates() {
  # TODO rethink args
  dpath_local="$(pwd)/data"
  gpath="${dpath_local}/genesis.json"
  epath="${dpath_local}/enodes.json"
  dcpath="${dpath_local}/docker-compose.yml"
  prompath="monitoring/prometheus/prometheus.yml"
    # TODO chainId hardcoded inside template maybe this var should be in another place
  . "${gpath}.template"
  cat > "${gpath}" <<EOF
$TEMPLATE
EOF
  . "${epath}.template"
  cat > "${epath}" <<EOF
$TEMPLATE
EOF
  . "${dcpath}.template"
  cat > "${dcpath}" <<EOF
$TEMPLATE
EOF
  . "${prompath}.template"
  cat > "${prompath}" <<EOF
$TEMPLATE
EOF
}

#####################

# allow safe use of relative paths
cd "$(dirname "${0}")"

# TODO check
#dpath="$(pwd)/data"

# TODO better variable handling
# https://github.com/hyperledger/besu/releases/tag/21.10.4
version=21.10.4

parse_arguments "${@}"
# TODO verify that besu image is downloaded
# TODO upgrade to a version safe with log4j
for node in $(seq 1 ${nodes_length}); do
  echo '\n\n\n------------------'
  echo "node${node}\n\n\n"
  # TODO allow skipping it with argument
  gen_keys "${node}"
done
gen_templates
make clean
