# all:
# 	TODO

test:
# TODO enable shellcheck
#	./test-with-shellcheck.sh
	# we ensure that temba is run in TEST mode
	shellspec --env TEST=true

# clean instance data besu's
besu_clean_all: besu_clean_keys besu_clean_db besu_clean_templates

# TODO network -> hosts

besu_clean_db:
	rm -rf db.d/network/*/database
	rm -rf db.d/network/*/caches
	rm -rf db.d/network/*/DATABASE_METADATA.json

besu_clean_keys:
	rm -f db.d/network/*/privkey
	rm -f db.d/network/*/pubkey
	rm -f db.d/network/*/enode

besu_clean_templates:
	rm -f db.d/network/*/enodes.json
	rm -f db.d/network/*/genesis.json
	rm -f db.d/network/*/remote_enodes.json
	rm -f db.d/network/*/docker-compose.yml

geth_clean_all: geth_clean_keys geth_clean_db geth_clean_templates

geth_clean_keys:
	rm -rf db.d/network/*/geth
	rm -rf db.d/network/*/keystore
	rm -rf db.d/network/*/geth_keys_generated
	rm -rf db.d/network/*/geth_pubkey
	rm -rf db.d/network/*/geth_enode

geth_clean:
	sudo rm -rf db.d/network/node*/*

geth_restart_clean:
	docker compose down -v
	$(MAKE) geth_clean
	sync_gen='n' ./bootstrap.sh
	docker compose up
	docker compose up
