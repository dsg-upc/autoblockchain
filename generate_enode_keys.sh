#!/bin/sh

#Executing:
#docker run -it --rm -v /path/to/node/datadir:/root/.ethereum:rw --entrypoint=/bin/sh ethereum/client-go:alltools-v1.10.16 /root/.ethereum/generate_enode_keys.sh

#password file needs to be present on the mounting directory

cd /root/.ethereum
mkdir geth
bootnode -genkey geth/nodekey
bootnode -nodekey geth/nodekey -writeaddress >> geth/enode

#maybe check if it exists before executing?
geth account new --password password
