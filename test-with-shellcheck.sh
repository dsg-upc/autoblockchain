#!/bin/sh -eu

# thanks from https://code.ungleich.ch/ungleich-public/cdist-contrib/src/branch/master/scripts/run-shellcheck.sh

#SHELLCHECKCMD='shellcheck -s sh -f gcc -x -e SC2154 -e SC2034'
SHELLCHECKTMP='.shellcheck.tmp'

rm -f "${SHELLCHECKTMP}"

# ignore some shellchecks ->
#   src https://github.com/koalaman/shellcheck/wiki/Ignore#ignoring-errors-in-one-specific-run
# SC2154: warning: TEMPLATE appears unused. Verify use (or export if used externally).
#   Not related
# SC2034: warning: myvariable is referenced but not assigned.
#   This is covered by shellspec
SC_CONF='shellcheck -s sh -f gcc -x -e SC2034'
# prune vim swp -> src https://github.com/remarkablemark/remarkablemark.github.io/blob/master/_posts/2017/2017-08-13-delete-swp-files.md
find -type f -prune -o -name '*.swp' \
  -exec ${SC_CONF} {} + \
  >>"${SHELLCHECKTMP}" || true

if test -s "${SHELLCHECKTMP}"; then
  cat "${SHELLCHECKTMP}" >&2
  exit 1
else
  rm -f "${SHELLCHECKTMP}"
fi
