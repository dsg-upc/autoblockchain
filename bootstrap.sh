#!/bin/sh -eu

#set -x

stderr() {
        printf "$@\n" >&2
}

AB_DEBUG() {
        [ -n "${AB_DEBUG}" ] && stderr "$@"
        true
}

# src https://unix.stackexchange.com/questions/23111/what-is-the-eval-command-in-bash/250345#250345
dbg_var() {
        local var="${1}"
        eval echo "${var}: \$${var}"
}

# generate docker-compose for remote scenario
# TODO generalizable?
gen_dc_remote() {
        dc_template='docker-compose.yml.template'
        dc_remote="${templates_output}/${dc_template}"
        cp -a "${besu_templates}/docker-compose.yml.template" "${dc_remote}"
        dc_remote_file="$(RM_SRC=true remote=true stu__template "${dc_remote}")"
        cp "${dc_remote_file}" "${node_path}/"
}

# this is "hardcoded" for besu
besu_gen_templates() {
        mkdir -p "${templates_output}"
        cp -a "${besu_templates}"/* "${templates_output}"
        # generates recursively all templates from templates_output
        RM_SRC=true stu__templates "${templates_output}"
        cp "${templates_output}/docker-compose.yml" "${templates_output}/local-docker-compose.yml"

        # propagate needed templates to launch docker-compose
        for keyfile in ${dpath:-db.d/network}/*/pubkey; do
                node_path="$(dirname "${keyfile}")"
                node_name="$(basename "${node_path}")"

                cp "${templates_output}/genesis.json" "${node_path}"
                cp "${templates_output}/enodes.json" "${node_path}"

                # generate docker compose per node for remote deployment
                gen_dc_remote
                # process remote_enodes.json
                #   TODO make it more clean
                cp "${besu_templates}/enodes.json.template" "${templates_output}/remote_enodes.json.template"
                remote_enodes_file="$(RM_SRC=true remote=true stu__template "${templates_output}/remote_enodes.json.template")"
                cp "${templates_output}/remote_enodes.json" "${node_path}"
        done
        # propagate docker-compose on git project directory for local deployment
        cp "${templates_output}/local-docker-compose.yml" 'docker-compose.yml'
}

geth_gen_templates() {
        mkdir -p "${templates_output}"
        cp -a "${geth_templates}"/* "${templates_output}"
        # generates recursively all templates from templates_output
        RM_SRC=true stu__templates "${templates_output}"
        # enable script
        chmod +x "${templates_output}/geth_start.sh"
        # TODO check
        #cp "${templates_output}/docker-compose.yml" "${templates_output}/local-docker-compose.yml"

        # propagate needed templates to launch docker-compose
        for keyfile in ${dpath:-db.d/network}/*/geth_pubkey; do
                node_path="$(dirname "${keyfile}")"
                node_name="$(basename "${node_path}")"

                cp "${templates_output}/genesis.json" "${node_path}/geth_genesis.json"
                cp "${templates_output}/static-nodes.json" "${node_path}/geth/"

                # TODO
                # generate docker compose per node for remote deployment
                #gen_dc_remote
                # process remote_enodes.json
                #   TODO make it more clean
                #cp "${besu_templates}/enodes.json.template" "${templates_output}/remote_enodes.json.template"
                #remote_enodes_file="$(RM_SRC=true remote=true stu__template "${templates_output}/remote_enodes.json.template")"
                # TODO for geth
                #cp "${templates_output}/remote_enodes.json" "${node_path}"
        done
        # propagate docker-compose on git project directory for local deployment
        cp "${templates_output}/docker-compose.yml" 'docker-compose.yml'
        # TODO I don't like this way
        cp "${templates_output}/geth_start.sh" 'geth_start.sh'
}

besu_gen_node_keys() {
        npath_local="$(pwd)/${node}"
        npath_docker="${dpath_docker}"

        if [ ! -f "${pubkey}" ] || [ ! -f "${privkey}" ] || [ ! -f "${enode}" ]; then
                echo "INFO: ${node}: missing ${dlt_target} key(s), regenerating..."
        else
                # keys already exist: do nothing
                return 0
        fi


        # TODO discussion, a selector could choose between besu, geth, etc.
        besu_cmd="docker run \
                -v ${npath_local}:${dpath_docker}:rw --rm \
                hyperledger/besu:${besu_version}"
        ${besu_cmd} \
                --data-path=${npath_docker} \
                public-key export-address --to=${npath_docker}/pubkey
        ${besu_cmd} \
                --data-path=${npath_docker} \
                public-key export --to=${npath_docker}/enode
        cd ${npath_local}
        mv key privkey
        cd - > /dev/null 2>&1
}

gen_passwd() {
        cat /dev/urandom | tr -dc 'a-zA-Z0-9-_!@#$%^&*()_+{}|:<>?=' | fold -w 32 | head -n 1
}

geth_gen_node_keys() {
        npath_local="$(pwd)/${node}"
        npath_docker="${geth_dpath_docker}"

        if [ ! -f "${geth_keys_generated:-}" ]; then
                echo "INFO: ${node}: missing ${dlt_target} key(s), regenerating..."
        else
                # keys already exist: do nothing
                return 0
        fi

        geth_secret_path="${npath_local}/${geth_passwd_file}"
        if [ ! -f "${geth_secret_path}" ]; then
                cat > "${geth_secret_path}" <<EOF
$(gen_passwd)
EOF
        fi

        # TODO do it this way in besu to run less containers
        # thanks https://riptutorial.com/docker/example/10313/passing-stdin-to-the-container
        # TODO permissions
        #   rm: cannot remove 'geth/nodekey': Permission denied
        #   rm: cannot remove 'geth/enode': Permission denied

        # geth/nodekey is used to generate enode key (don't rename nodekey, it is needed on execution)
        # keystore is equivalent to privkey

        docker run -i --rm \
                -v ${npath_local}:${geth_dpath_docker}:rw \
                ${geth_image}:alltools-${geth_version} /bin/sh -e <<END
cd ${geth_dpath_docker}
mkdir -p geth
bootnode -genkey geth/nodekey
bootnode -nodekey geth/nodekey -writeaddress > geth_enode

#maybe check if it exists before executing?
geth account new --password "${geth_passwd_file}"
touch geth_keys_generated
# TODO improve me :(
chmod -R 777 ${geth_dpath_docker}
END
        # TODO fail if jq program is not available
        cat "${npath_local}/keystore/"* | jq -r '.address' > "${npath_local}/geth_pubkey"
        # TODO why is geth_cmd needed?
        #echo "${geth_cmd}"
}

# TODO by default we are going to preserve node keys
#   and put a make method to delete keys: make clean_keys
gen_all_keys() {
        # TODO improve validation
        nodes="${1}"
        AB_DEBUG '\n\ngen_all_keys() started\n\n'
        while IFS= read -r node <&9; do

                AB_DEBUG "\n___gen_all_keys() iteration__\n\nnode: ${node}\n\n"

                sdbu__read_obj "${node}"

                case "${dlt_target}" in
                        besu)
                                if [ "${async}" = 'y' ]; then
                                        besu_gen_node_keys "${node}" &
                                else
                                        besu_gen_node_keys "${node}"
                                fi
                                ;;
                        geth)
                                if [ "${async}" = 'y' ]; then
                                        geth_gen_node_keys "${node}" &
                                else
                                        geth_gen_node_keys "${node}"
                                fi
                                ;;
                        *)
                                stderr 'ERROR: var dlt_target not defined, fill it with besu or geth'
                                exit 1
                                ;;
                esac

        done 9<<END
${nodes}
END
        # wait all background processes to finish (if any)
        #   extra src https://unix.stackexchange.com/questions/344360/collect-exit-codes-of-parallel-background-processes-sub-shells
        wait
        AB_DEBUG '\n\ngen_all_keys() finished\n\n'
        true
}

main() {
        # allow safe use of relative paths
        cd "$(dirname "${0}")"

        # TODO db.d/hosts
        # TODO net_file -> host_file
        net_file='db.d/network'
        if [ ! -d "${net_file}" ]; then
                echo 'no network directory detected, copying example network for a quick demo'
                cp -av "db.d/network.example" "db.d/network"
        fi

        AB_DEBUG="${AB_DEBUG:-}"

        . shell-template-utils/stu.sh
        . shell-db.d-utils/sdbu.sh

        #for node in $(seq 1 ${nodes_length}); do
        # follow link because network could be a symlink
        # TODO nodes -> hosts
        nodes="$(find -L "${net_file}" -maxdepth 1 -type d \
            | sort | grep -v "^${net_file}$" )"
        [ -n "${AB_DEBUG}" ] && dbg_var nodes

        # that's debug
        #AB_DEBUG=1 gen_all_keys "${nodes}"
        gen_all_keys "${nodes}"

        # TODO switch between templates
        #   besu_gen_templates
        geth_gen_templates
}

main
