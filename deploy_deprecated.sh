#!/bin/sh -e

# alternate deploy
# TODO facility to deploy more nodes

# src https://en.wikipedia.org/wiki/Usage_message
usage() {
  echo "Usage: ${0} -n node_number [-g ]"
  exit 1
}

parse_args() {
  # src https://stackoverflow.com/questions/16483119/an-example-of-how-to-use-getopts-in-bash/49573433#49573433
  #   https://pubs.opengroup.org/onlinepubs/9699919799/utilities/getopts.html
  #     If a character is followed by a <colon>, the option shall be expected to have an argument
  while getopts n:g name
  do
    case $name in
      n) nflag=1
         NODE="$OPTARG";;
      g) gflag=1;;
      ?) usage
      exit 2;;
    esac
  done
  if [ -z "$nflag" ]; then
    usage
  fi
  if [ -n "${gflag}" ]; then
    GENESIS='y'
  else
    GENESIS='n'
  fi
}

#############################

# allow safe use of relative paths
cd "$(dirname "${0}")"
dpath="$(pwd)/data"

parse_args "${@}"

# debug
# echo docker run \

# note: entrypoint is injected in data so we don't need to modify the image

docker run \
  --name "${node_prefix:-node}${NODE}" \
  -v ${dpath}:/opt/besu/data:rw \
  -e NODE=${NODE} -e GENESIS=${GENESIS} \
  -it -P --rm -h "${node_prefix:-node}${NODE}" \
  --entrypoint=/opt/besu/data/entrypoint.sh \
    hyperledger/besu:21.7.4

# debug
#  --entrypoint=/bin/bash \
