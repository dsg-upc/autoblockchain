#!/bin/sh -eu

# debug
echo "NODE = ${NODE}"
echo "GENESIS = ${GENESIS}"

dpath="/opt/besu/data"
if ! [ ${NODE} -gt 0 ] 2> /dev/null; then
  echo "Node should be a number such as 1, 2, 3, etc."
  exit 1
fi
npath="${dpath}/${NODE}"
gpath="${dpath}/genesis.json"
epath="${dpath}/enodes.json"

base_port=3000
# data generated once
if [ ! -d "${npath}" ]; then
  mkdir "${npath}"
  besu --data-path=${npath} \
    public-key export-address --to=${npath}/pubkey
  besu --data-path=${npath} \
    public-key export --to=${npath}/enode
  cat > ${npath}/port <<EOF
$((base_port + NODE))
EOF
  cd ${npath}
  # TODO va a dar problemas???
  mv key privkey
  cd -
fi

# wait to all keys to be generated
#sleep 5

# TODO another approach would be to know all nodes that are going to
#   participate and wait for them to finish the key generation step
#   hence: traverse and check all files that are going to participate
#     we need anyway to traverse to get all keys

# node with the genesis flag is responsible to generate genesis
if [ "${GENESIS}" = "y" ]; then
  # TODO chainId hardcoded inside template maybe this var should be in another place
  . "${gpath}.sh"
  cat > "${gpath}" <<EOF
$TEMPLATE
EOF
  . "${epath}.sh"
  cat > "${epath}" <<EOF
$TEMPLATE
EOF
fi

# TODO chainId not defined when no bootstrap
chainId=1337

# about Xdns options -> src https://besu.hyperledger.org/en/stable/Concepts/Node-Keys
besu --genesis-file="${gpath}" \
  --static-nodes-file="${epath}" \
  --discovery-enabled=false \
  --network-id="$chainId" \
  --Xdns-enabled=true \
  --Xdns-update-enabled=true
