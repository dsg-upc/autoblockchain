#!/bin/sh -e

#set -x

stderr() {
  printf "$@\n" >&2
}

AB_DEBUG() {
  [ -n "${AB_DEBUG}" ] && stderr "$@"
  true
}

# src https://unix.stackexchange.com/questions/23111/what-is-the-eval-command-in-bash/250345#250345
dbg_var() {
  local var="${1}"
  eval echo "${var}: \$${var}"
}

# generate docker-compose for remote scenario
# TODO generalizable?
gen_dc_remote() {
  dc_template='docker-compose.yml.template'
  dc_remote="${templates_output}/${dc_template}"
  cp -a "${besu_templates}/docker-compose.yml.template" "${dc_remote}"
  dc_remote_file="$(RM_SRC=true remote=true stu__template "${dc_remote}")"
  cp "${dc_remote_file}" "${node_path}/"
}

# this is "hardcoded" for besu
gen_templates() {
  mkdir -p "${templates_output}"
  cp -a "${besu_templates}"/* "${templates_output}"
  # generates recursively all templates from templates_output
  RM_SRC=true stu__templates "${templates_output}"
  cp "${templates_output}/docker-compose.yml" "${templates_output}/local-docker-compose.yml"

  # propagate needed templates to launch docker-compose
  for keyfile in ${dpath:-db.d/network}/*/pubkey; do
    node_path="$(dirname "${keyfile}")"
    node_name="$(basename "${node_path}")"

    cp "${templates_output}/genesis.json" "${node_path}"
    cp "${templates_output}/enodes.json" "${node_path}"

    # generate docker compose per node for remote deployment
    gen_dc_remote
    # process remote_enodes.json
    #   TODO make it more clean
    cp "${besu_templates}/enodes.json.template" "${templates_output}/remote_enodes.json.template"
    remote_enodes_file="$(RM_SRC=true remote=true stu__template "${templates_output}/remote_enodes.json.template")"
    cp "${templates_output}/remote_enodes.json" "${node_path}"
  done
  # propagate docker-compose on git project directory for local deployment
  cp "${templates_output}/local-docker-compose.yml" 'docker-compose.yml'
}

# TODO backups de llaves anteriores con timestamp
# TODO parallelize key generation process, background del proceso? potencial
#   problema de que se haga template sin generación de llaves
# TODO rework node var, that var is no longer a number
gen_node_keys() {
  npath_local="$(pwd)/${node}"
  npath_docker="${dpath_docker}"

  # TODO discussion, a selector could choose between besu, geth, etc.
  besu_cmd="docker run \
    -v ${npath_local}:${dpath_docker}:rw --rm \
      hyperledger/besu:${version}"
  ${besu_cmd} \
    --data-path=${npath_docker} \
    public-key export-address --to=${npath_docker}/pubkey
  ${besu_cmd} \
    --data-path=${npath_docker} \
    public-key export --to=${npath_docker}/enode
  cd ${npath_local}
  mv key privkey
  cd - > /dev/null 2>&1
}

# TODO by default we are going to preserve node keys
#   and put a make method to delete keys: make clean_keys
gen_all_keys() {
  # TODO improve validation
  nodes="${1}"
  AB_DEBUG '\n\ngen_all_keys() started\n\n'
  while IFS= read -r node <&9; do

    AB_DEBUG "\n___gen_all_keys() iteration__\n\nnode: ${node}\n\n"

    sdbu__read_obj "${node}"

    if [ ! -f "${pubkey}" ] || [ ! -f "${privkey}" ] || [ ! -f "${enode}" ]; then
      echo "INFO: ${node}: missing key(s), regenerating..."
      # using async version
      gen_node_keys "${node}" &
      # if that eats up a lot of RAM, uncomment the sync version (1 by 1)
      #gen_node_keys "${node}"
    fi

  done 9<<END
${nodes}
END
  # wait all background processes to finish (if any)
  #   extra src https://unix.stackexchange.com/questions/344360/collect-exit-codes-of-parallel-background-processes-sub-shells
  wait
  AB_DEBUG '\n\ngen_all_keys() finished\n\n'
  true
}

main() {
  # allow safe use of relative paths
  cd "$(dirname "${0}")"

  net_file='db.d/network'
  if [ ! -d "${net_file}" ]; then
    echo 'no network directory detected, copying example network for a quick demo'
    cp -av "db.d/network.example" "db.d/network"
  fi

  AB_DEBUG="${AB_DEBUG:-}"

  # TODO check
  #dpath="$(pwd)/data"

  #parse_arguments "${@}"

  . shell-template-utils/stu.sh
  . shell-db.d-utils/sdbu.sh

  # TODO test/verify that besu image is downloaded
  #for node in $(seq 1 ${nodes_length}); do
  # follow link because network could be a symlink
  nodes="$(find -L "${net_file}" -maxdepth 1 -type d \
    | sort | grep -v "^${net_file}$" )"
  [ -n "${AB_DEBUG}" ] && dbg_var nodes

  # that's debug
  #AB_DEBUG=1 gen_all_keys "${nodes}"
  gen_all_keys "${nodes}"

  gen_templates
  # TODO uncomment
  #make clean
}

main
