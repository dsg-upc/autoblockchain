#!/bin/sh -e

# thanks https://www.atlassian.com/git/tutorials/git-subtree

manage_gitst() {
  path="${1}"
  repo="${2} ${3}"
  if [ ! -d "${path}" ]; then
    action="add"
  else
    action="pull"
  fi
  cmd="git subtree ${action} --prefix ${path} ${repo} --squash"
  ${cmd}
}

manage_gitst 'shell-db.d-utils' 'https://gitlab.com/dsg-upc/shell-db.d-utils.git main'
manage_gitst 'shell-template-utils' 'https://gitlab.com/dsg-upc/shell-template-utils.git main'
