#!/bin/sh

set -eu

stderr() {
  printf "$@\n" >&2
}

AB_DEBUG() {
  [ -n "${AB_DEBUG}" ] && stderr "$@"
  true
}

check_ssh_key() {
  ssh-add -l > /dev/null 2>&1 || {
    stderr 'ERROR: no ssh pubkey present, use `ssh-add ~/.ssh/your_privkey`'
    exit 1
  }
}

prepare_reception() {
  ssh "${target_host}" -- sh -e <<END
if ! command -v rsync >/dev/null; then
  apt install -y rsync
fi
rm -rf ${remote_path}
mkdir -p ${remote_path}/${node_path}
ln -sf ${remote_path}/${node_path}/docker-compose.yml ${remote_path}/docker-compose.yml
END
}

main() {

  # get general vars
  . db.d/cfg.env
  # enforce specific remote_path for caution
  #   TODO prepare_reception() contains an rm -rf of remote_path, find a better way
  readonly remote_path="/opt/autoblockchain"

  check_ssh_key

  nodes="$(dirname db.d/network/*/cfg.env)"
  for node_path in ${nodes}; do
    echo "processing ${node_path}"
    # load node vars (ssh host)
    . "${node_path}/cfg.env"
    target_host="${remote_user}@${ssh_host}"

    prepare_reception

    filter_list=''
    for target_file in ${remote_files}; do
      filter_list="${filter_list} ${node_path}/${target_file}"
    done
    cmd="rsync ${filter_list} ${target_host}:${remote_path}/${node_path}"
    ${cmd}
  done
}

main
